package com.example.conversion;

public class Converter {

    public double toF(String cel) {
        return Integer.parseInt(cel) * 1.8 + 32;
    }
}
